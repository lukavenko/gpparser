﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPlayParser
{
    public class Data
    {
        public string Category { get; set; }

        public string Link { get; set; }

        public string Name { get; set; }
        public string DownloadCount { get; set; }
        public string FBLink { get; set; }
        public string VKLink { get; set; }
        public string DeveloperEmail { get; set; }
        public string DeveloperAddress { get; set; }
        public string DeveloperWebsite { get; set; }
        public string Description { get; set; }
    }
}
