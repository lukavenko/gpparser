﻿using AngleSharp.Parser.Html;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace GPlayParser
{
    public class Parser
    {
        public static List<Data> ParseCategory1(string url)
        {
            var list = new List<Data>();

            // Первый проход - собираем ссылки на индивидуальные страницы игр
            // Собираем все товары на данной странице
            using (WebClient remote = new WebClient())
            {
                #region
                //Console.WriteLine("goto: " + url);
                //remote.Headers[":authority"] = "play.google.com";
                //remote.Headers[":method"] = "POST";
                //remote.Headers[":path"] = "/store/apps/collection/promotion_30017ea_starterkit_games?clp=Sp4BCigKInByb21vdGlvbl8zMDAxN2VhX3N0YXJ0ZXJraXRfZ2FtZXMQBxgDEgRHQU1FGmwKZm5ld19ob21lX2RldmljZV9mZWF0dXJlZF9yZWNzMl90b3BpY192MV9sYXVuY2hfR0FNRV9hcHBzX19fR0FNRV8xNDk5MzY3NjAwMDAwXzFfcHJvbW9fMTUxMDg2MTM1NzMyNjAwMBAMGAM%3D%3AS%3AANO1ljKq_P8&authuser=0";
                //remote.Headers[":scheme"] = "https";
                remote.Headers["accept"] = "*/*";
                remote.Headers["accept-encoding"] = "utf-8";
                remote.Headers["accept-language"] = "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7";
                remote.Headers["cache-control"] = "no-cache";
                //remote.Headers["content-length"] = "61";
                remote.Headers["content-type"] = "application/x-www-form-urlencoded;charset=UTF-8";
                remote.Headers["cookie"] = "CONSENT=YES+RU.ru+20170625-09-0; SID=_gR24lqY-_JnAiwlocdmpKNnRwlkJqybv1yU_804v29H6xdnBQShoSIiYQ5wINzfsz3_KA.; HSID=AbHvAtZO0PNF6sT0r; SSID=AaxsU5KojHUuTXjJs; APISID=R_R4TCkVa8lxzp4-/AjouDZrOqr_DfASer; SAPISID=IewzeoMkXYQ1T2Vl/Aj2D0SrW1GLScvFht; NID=118=Tyd-co8HUvTR_22edkj-NsMqXThNR9Em10rQ4kEe1AZ-zPqHaNDX1V4XGbyiyWTBGyYwKaBTeB3aBG9e_IE4-knpNnpTtNNeCLemiln8LdClsJBb_2sa__sIvW9gCzedKSi8I5jy0Q9kP2G5IyMwlEmZwh1amFusdKa4y67Ij9do2NKlHSO5nOwD3vUd15WtccqYFlZmSxXARnPGhjs1GMVImKY5M6ivX8GSzKEbp1R4nuPEEKPv2uuE_FN-hK2dJsnzG5ZhOsM3KxDM2EToz-tL; S=billing-ui-v3=A3-dw0i6wBLh_7nKdTBpCse9w3kD4tWa:billing-ui-v3-efe=A3-dw0i6wBLh_7nKdTBpCse9w3kD4tWa; PLAY_PREFS=CtgKCPbEi6fTCRLOCgoCUlUQwomCloAsGo8KERITFBUWGNQB1QGrAuEDwgTjBeUF6AXXBtgG3gbfBpCVgQaRlYEGkpWBBpWVgQaXlYEGpJWBBriVgQbAlYEGwZWBBsSVgQbFlYEGyJWBBs6VgQbPlYEG0JWBBtSVgQbYlYEG8ZWBBviVgQb5lYEGhpaBBoeWgQaMloEGjpaBBpGWgQaeloEGn5aBBqGWgQakloEGppaBBqeWgQbul4EG75eBBoGYgQaFmIEGiZiBBouYgQa-mIEGq5uBBq2bgQbJm4EGypuBBsubgQbVm4EGvJ2BBt2dgQbnnYEGkJ6BBuKigQbzooEG_KKBBoujgQaapIEGsqSBBuqlgQbGpoEG1KaBBtamgQbPp4EGvKyBBtavgQaHsoEGibKBBtaygQaxtIEG1rmBBo7AgQaiwIEGwMCBBsHAgQbywIEG1sKBBozFgQaPxYEG-MeBBqrKgQbYzIEG3MyBBt3NgQaGzoEGoc-BBsTSgQaV1YEG2tiBBuLYgQaT2YEGy9mBBvLbgQbY5IEG3OSBBpflgQa46IEGz-uBBrDsgQbX9YEGuvuBBr__gQbE_4EGyP-BBtWDggbIhIIGuYaCBqaHgganh4IG7IeCBu2Hggb7jYIGiY6CBsyRggaVmIIGj5qCBpmaggbBmoIG95qCBp2eggbVnoIGuqCCBrugggb2ooIG4qSCBpKlggarpYIGzaWCBvKnggaeqIIGtKiCBoG0ggaDtIIGhrSCBq22ggbCu4IGj7-CBurAgga8wYIGkMuCBpHLggbNy4IG0cuCBtzMggbY0IIG89GCBtvTggaB2IIGhNqCBo7aggab2oIGo9qCBq3bggau24IGxduCBpfcggax3IIG6t2CBvjdggbv34IGp-GCBtDhggbk4YIG5eGCBoXoggaW6YIGhuuCBqPtggaF7oIGmu6CBrPuggaM8IIGsfCCBpbxgga-8YIG6_aCBq34ggaz-IIG9vqCBt77ggbf-4IG4_uCBoX8ggbC_IIG2_yCBtz8ggb__IIGgP2CBoH9ggaC_YIGgv-CBoCAgwbcgYMG8oGDBuOEgwbqhIMGkIWDBrmFgwabiIMGnYiDBtCIgwbwiIMG842DBoWPgwaQj4MGz5CDBt-QgwbZkYMG25GDBv6Rgwbbk4MG4pODBquVgwaslYMGuJWDBp2WgwaeloMGpJaDBsCWgwbWloMG45aDBtyXgwbkmYMG55mDBtuagwaZm4MG7ZuDBu6bgwbjnIMG9J6DBpWfgwbGn4MG05-DBtmfgwaYoIMGm6CDBrOggwb9oIMGwqGDBsyhgwbNoYMG1qKDBrmjgwbMp4MG7aiDBvOogwb_q4MG7K-DBvuvgwb9r4MGlrCDBrixgwa9sYMGvLSDBtC1gwa4toMGo7eDBqW4gwauuIMGubiDBsO4gwbgvIMG9LyDBvW8gwb2vIMGr76DBs2-gwbnv4MGw8GDBvnBgwb6wYMGycKDBo7DgwauxIMG5MaDBq3IgwbNyIMGnsmDBovKgwabyoMGnMqDBo_Lgwbzy4MGkMyDBsTMgwaHzYMG-M2DBofOgwaUzoMGl86DBunOgwaAz4MG6NCDBrrRgwb504MGgdSDBtzUgwbr1IMGttaDBsDWgwbX1oMG_daDBorXgwaa2IMGk9qDBqTagwau2oMGj9uDBondgwaK3YMGjt-DBpzhgwaK4oMGm-KDBqHigwby4oMGouODBrjjgwbo5IMGh-aDBuXogwYojJDR9f8rOiRjNTAxZWM3ZS02NDhjLTQ0YmEtOGU3ZS02M2FmYTNlNjcyMDlAAUgA:S:ANO1ljL4ErkTQ4_DaA; _ga=GA1.3.734552973.1493814740; _gid=GA1.3.731475328.1511806781; 1P_JAR=2017-11-28-13; SIDCC=AE4kn7829Lu-kJeYSKDaYQgZxMm1iI4ru46wsi8BF--YxK-xZAzc--FyWubM4Qp60w9jaQpH8FKWJMSuDIy74Q; _gat=1";
                remote.Headers["origin"] = "https://play.google.com";
                remote.Headers["pragma"] = "no-cache";
                remote.Headers["referer"] = "https://play.google.com/store/apps/collection/promotion_30017ea_starterkit_games?clp=Sp4BCigKInByb21vdGlvbl8zMDAxN2VhX3N0YXJ0ZXJraXRfZ2FtZXMQBxgDEgRHQU1FGmwKZm5ld19ob21lX2RldmljZV9mZWF0dXJlZF9yZWNzMl90b3BpY192MV9sYXVuY2hfR0FNRV9hcHBzX19fR0FNRV8xNDk5MzY3NjAwMDAwXzFfcHJvbW9fMTUxMDg2MTM1NzMyNjAwMBAMGAM%3D:S:ANO1ljKq_P8";
                remote.Headers["user-agent"] = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36";
                remote.Headers["x-chrome-uma-enabled"] = "1";
                remote.Headers["x-client-data"] = "CIi2yQEIpbbJAQjEtskBCPqcygEIqZ3KAQioo8oB";

                #endregion

                // Будет ли запрошена следующая страница
                bool isNextPage = true;
                int page = 0;
                string requestDataTemplate = "ipf=1&xhr=1&token=uMHUPFwvNv1DNf0CdJ3MtRYJZ-s%3A1511939780883";
                var requestData = requestDataTemplate;

                //while (isNextPage)
                //{
                //    // Для второй страницы и далее url Другой
                //    if(page > 0)
                //    {
                //        Uri uri = new Uri(url);
                //        url = "https://play.google.com" + uri.AbsolutePath + "?authuser=0";
                //    }

                    var htmlParser = new HtmlParser();
                    byte[] byteArray = Encoding.UTF8.GetBytes(requestData);
                    byte[] responseArray = remote.UploadData(url, "POST", byteArray);                //var document = htmlParser.Parse(html);
                    var html = Encoding.UTF8.GetString(responseArray);
                    var document = htmlParser.Parse(html);

                    foreach (var item in document.QuerySelectorAll(".card-click-target"))
                    {
                        list.Add(new Data()
                        {
                            Link = item.GetAttribute("href")
                        });

                        // todo: remove
                        break;
                    }

                    // Берем следующую страницу
                    //page++;
                    //requestData = requestDataTemplate + "&start=" + (page * 48) + "&num=" + (page * 48);
                //}
            };

            // Ищем только новинки
            var reestr = LoadReestr();
            list = list.Distinct(new DataEqualityComparer()).Where(v => !reestr.Contains(v.Link)).ToList();

            // Второй проход - сбор детальной информации
            var i = 1;
            foreach (var item in list)
            {
                Console.WriteLine(string.Format("parsing {0} from {1}", i, list.Count));

                using (WebClient remote = new WebClient())
                {
                    remote.Encoding = Encoding.UTF8;
                    var htmlParser = new HtmlParser();
                    var document = htmlParser.Parse(remote.DownloadString("https://play.google.com" + item.Link));

                    var nameElement = document.QuerySelector(".id-app-title");
                    if (nameElement != null)
                        item.Name = nameElement.TextContent;

                    foreach (var metaItem in document.QuerySelectorAll(".details-section-contents .meta-info"))
                    {
                        var title = metaItem.QuerySelector(@"[class=""title""]");
                        var content = metaItem.QuerySelector(@"[class=""content""]");
                        var contentLinks = metaItem.QuerySelector(@"[class=""content contains-text-link""]");

                        if (title != null && title.TextContent == "Количество установок" && content != null)
                        {
                            item.DownloadCount = content.TextContent;
                        }

                        if (title != null && title.TextContent == "Разработчик" && contentLinks != null)
                        {
                            // Почта
                            var emailElement = contentLinks.QuerySelector(@"[href^=""mailto:""]");
                            if(emailElement != null)
                            {
                                item.DeveloperEmail = (emailElement.GetAttribute("href") + "").Replace("mailto:", "");
                            }

                            // Сайт разработчика
                            var websiteElement = contentLinks.QuerySelector(@"[href^=""https://www.google.com/url""]");
                            if (websiteElement != null)
                            {
                                var websiteLink = websiteElement.GetAttribute("href");
                                if(!string.IsNullOrWhiteSpace(websiteLink))
                                {
                                    // Ex: https://www.google.com/url?q=http://supercell.helpshift.com/a/clash-royale/&sa=D&usg=AFQjCNEcjDY02MgHR333A8cRg06Wix3jsA
                                    var websiteUri = new Uri(websiteLink);
                                    item.DeveloperWebsite = HttpUtility.ParseQueryString(websiteUri.Query).Get("q");
                                }
                            }

                            // Адрес
                            var addressElement = contentLinks.QuerySelector(@"[class=""content physical-address""]");
                            if (addressElement != null)
                            {
                                item.DeveloperAddress = (addressElement.TextContent + "").Replace(char.ConvertFromUtf32(10), " ");
                            }
                        }
                    }

                    var descriptionElement = document.QuerySelector(".show-more-content.text-body");
                    if (descriptionElement != null)
                        item.Description = descriptionElement.TextContent;
                };

                i++;

                // todo: remove
                //break;
            }

            return list;
        }

        public static List<Data> ParseCategory2(string url)
        {
            var list = new List<Data>();

            // Первый проход - собираем ссылки на индивидуальные страницы игр
            // Собираем все товары на данной странице
            using (WebClient remote = new WebClient())
            {
                #region
                //Console.WriteLine("goto: " + url);
                //remote.Headers[":authority"] = "play.google.com";
                //remote.Headers[":method"] = "POST";
                //remote.Headers[":path"] = "/store/apps/collection/promotion_30017ea_starterkit_games?clp=Sp4BCigKInByb21vdGlvbl8zMDAxN2VhX3N0YXJ0ZXJraXRfZ2FtZXMQBxgDEgRHQU1FGmwKZm5ld19ob21lX2RldmljZV9mZWF0dXJlZF9yZWNzMl90b3BpY192MV9sYXVuY2hfR0FNRV9hcHBzX19fR0FNRV8xNDk5MzY3NjAwMDAwXzFfcHJvbW9fMTUxMDg2MTM1NzMyNjAwMBAMGAM%3D%3AS%3AANO1ljKq_P8&authuser=0";
                //remote.Headers[":scheme"] = "https";
                remote.Headers["accept"] = "*/*";
                remote.Headers["accept-encoding"] = "utf-8";
                remote.Headers["accept-language"] = "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7";
                remote.Headers["cache-control"] = "no-cache";
                //remote.Headers["content-length"] = "61";
                remote.Headers["content-type"] = "application/x-www-form-urlencoded;charset=UTF-8";
                remote.Headers["cookie"] = "CONSENT=YES+RU.ru+20170625-09-0; SID=_gR24lqY-_JnAiwlocdmpKNnRwlkJqybv1yU_804v29H6xdnBQShoSIiYQ5wINzfsz3_KA.; HSID=AbHvAtZO0PNF6sT0r; SSID=AaxsU5KojHUuTXjJs; APISID=R_R4TCkVa8lxzp4-/AjouDZrOqr_DfASer; SAPISID=IewzeoMkXYQ1T2Vl/Aj2D0SrW1GLScvFht; NID=118=Tyd-co8HUvTR_22edkj-NsMqXThNR9Em10rQ4kEe1AZ-zPqHaNDX1V4XGbyiyWTBGyYwKaBTeB3aBG9e_IE4-knpNnpTtNNeCLemiln8LdClsJBb_2sa__sIvW9gCzedKSi8I5jy0Q9kP2G5IyMwlEmZwh1amFusdKa4y67Ij9do2NKlHSO5nOwD3vUd15WtccqYFlZmSxXARnPGhjs1GMVImKY5M6ivX8GSzKEbp1R4nuPEEKPv2uuE_FN-hK2dJsnzG5ZhOsM3KxDM2EToz-tL; S=billing-ui-v3=A3-dw0i6wBLh_7nKdTBpCse9w3kD4tWa:billing-ui-v3-efe=A3-dw0i6wBLh_7nKdTBpCse9w3kD4tWa; PLAY_PREFS=CtgKCPbEi6fTCRLOCgoCUlUQwomCloAsGo8KERITFBUWGNQB1QGrAuEDwgTjBeUF6AXXBtgG3gbfBpCVgQaRlYEGkpWBBpWVgQaXlYEGpJWBBriVgQbAlYEGwZWBBsSVgQbFlYEGyJWBBs6VgQbPlYEG0JWBBtSVgQbYlYEG8ZWBBviVgQb5lYEGhpaBBoeWgQaMloEGjpaBBpGWgQaeloEGn5aBBqGWgQakloEGppaBBqeWgQbul4EG75eBBoGYgQaFmIEGiZiBBouYgQa-mIEGq5uBBq2bgQbJm4EGypuBBsubgQbVm4EGvJ2BBt2dgQbnnYEGkJ6BBuKigQbzooEG_KKBBoujgQaapIEGsqSBBuqlgQbGpoEG1KaBBtamgQbPp4EGvKyBBtavgQaHsoEGibKBBtaygQaxtIEG1rmBBo7AgQaiwIEGwMCBBsHAgQbywIEG1sKBBozFgQaPxYEG-MeBBqrKgQbYzIEG3MyBBt3NgQaGzoEGoc-BBsTSgQaV1YEG2tiBBuLYgQaT2YEGy9mBBvLbgQbY5IEG3OSBBpflgQa46IEGz-uBBrDsgQbX9YEGuvuBBr__gQbE_4EGyP-BBtWDggbIhIIGuYaCBqaHgganh4IG7IeCBu2Hggb7jYIGiY6CBsyRggaVmIIGj5qCBpmaggbBmoIG95qCBp2eggbVnoIGuqCCBrugggb2ooIG4qSCBpKlggarpYIGzaWCBvKnggaeqIIGtKiCBoG0ggaDtIIGhrSCBq22ggbCu4IGj7-CBurAgga8wYIGkMuCBpHLggbNy4IG0cuCBtzMggbY0IIG89GCBtvTggaB2IIGhNqCBo7aggab2oIGo9qCBq3bggau24IGxduCBpfcggax3IIG6t2CBvjdggbv34IGp-GCBtDhggbk4YIG5eGCBoXoggaW6YIGhuuCBqPtggaF7oIGmu6CBrPuggaM8IIGsfCCBpbxgga-8YIG6_aCBq34ggaz-IIG9vqCBt77ggbf-4IG4_uCBoX8ggbC_IIG2_yCBtz8ggb__IIGgP2CBoH9ggaC_YIGgv-CBoCAgwbcgYMG8oGDBuOEgwbqhIMGkIWDBrmFgwabiIMGnYiDBtCIgwbwiIMG842DBoWPgwaQj4MGz5CDBt-QgwbZkYMG25GDBv6Rgwbbk4MG4pODBquVgwaslYMGuJWDBp2WgwaeloMGpJaDBsCWgwbWloMG45aDBtyXgwbkmYMG55mDBtuagwaZm4MG7ZuDBu6bgwbjnIMG9J6DBpWfgwbGn4MG05-DBtmfgwaYoIMGm6CDBrOggwb9oIMGwqGDBsyhgwbNoYMG1qKDBrmjgwbMp4MG7aiDBvOogwb_q4MG7K-DBvuvgwb9r4MGlrCDBrixgwa9sYMGvLSDBtC1gwa4toMGo7eDBqW4gwauuIMGubiDBsO4gwbgvIMG9LyDBvW8gwb2vIMGr76DBs2-gwbnv4MGw8GDBvnBgwb6wYMGycKDBo7DgwauxIMG5MaDBq3IgwbNyIMGnsmDBovKgwabyoMGnMqDBo_Lgwbzy4MGkMyDBsTMgwaHzYMG-M2DBofOgwaUzoMGl86DBunOgwaAz4MG6NCDBrrRgwb504MGgdSDBtzUgwbr1IMGttaDBsDWgwbX1oMG_daDBorXgwaa2IMGk9qDBqTagwau2oMGj9uDBondgwaK3YMGjt-DBpzhgwaK4oMGm-KDBqHigwby4oMGouODBrjjgwbo5IMGh-aDBuXogwYojJDR9f8rOiRjNTAxZWM3ZS02NDhjLTQ0YmEtOGU3ZS02M2FmYTNlNjcyMDlAAUgA:S:ANO1ljL4ErkTQ4_DaA; _ga=GA1.3.734552973.1493814740; _gid=GA1.3.731475328.1511806781; 1P_JAR=2017-11-28-13; SIDCC=AE4kn7829Lu-kJeYSKDaYQgZxMm1iI4ru46wsi8BF--YxK-xZAzc--FyWubM4Qp60w9jaQpH8FKWJMSuDIy74Q; _gat=1";
                remote.Headers["origin"] = "https://play.google.com";
                remote.Headers["pragma"] = "no-cache";
                remote.Headers["referer"] = "https://play.google.com/store/apps/collection/promotion_30017ea_starterkit_games?clp=Sp4BCigKInByb21vdGlvbl8zMDAxN2VhX3N0YXJ0ZXJraXRfZ2FtZXMQBxgDEgRHQU1FGmwKZm5ld19ob21lX2RldmljZV9mZWF0dXJlZF9yZWNzMl90b3BpY192MV9sYXVuY2hfR0FNRV9hcHBzX19fR0FNRV8xNDk5MzY3NjAwMDAwXzFfcHJvbW9fMTUxMDg2MTM1NzMyNjAwMBAMGAM%3D:S:ANO1ljKq_P8";
                remote.Headers["user-agent"] = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36";
                remote.Headers["x-chrome-uma-enabled"] = "1";
                remote.Headers["x-client-data"] = "CIi2yQEIpbbJAQjEtskBCPqcygEIqZ3KAQioo8oB";

                #endregion

                // Будет ли запрошена следующая страница
                bool isNextPage = true;
                int page = 0;
                string requestDataTemplate = "start=48&num=48&numChildren=0&pagTok=gtP_uANLCkL6noGdAzwIMRDi6-aJCxDn2bZ0EIyGrfQIEMqDne0GEIeV-G8QptTc1AEQvK3VzgwQ3ceDwwIQwPO53wsQi5_aogQQwu-utIAs%3AS%3AANO1ljJXM3E&clp=ygIICgRHQU1FEAU%3D%3AS%3AANO1ljIRr9I&pagtt=3&cctcss=square-cover&cllayout=NORMAL&ipf=1&xhr=1&token=Yzd4XJbGApH6HfZ7jgy3VwSpNKQ%3A1511938297992";
                var requestData = requestDataTemplate;

                //while (isNextPage)
                //{
                //    // Для второй страницы и далее url Другой
                //    if(page > 0)
                //    {
                //        Uri uri = new Uri(url);
                //        url = "https://play.google.com" + uri.AbsolutePath + "?authuser=0";
                //    }

                var htmlParser = new HtmlParser();
                byte[] byteArray = Encoding.UTF8.GetBytes(requestData);
                byte[] responseArray = remote.UploadData(url, "POST", byteArray);                //var document = htmlParser.Parse(html);
                var html = Encoding.UTF8.GetString(responseArray);
                var document = htmlParser.Parse(html);

                foreach (var item in document.QuerySelectorAll(".card-click-target"))
                {
                    list.Add(new Data()
                    {
                        Link = item.GetAttribute("href")
                    });

                    // todo: remove
                    break;
                }

                // Берем следующую страницу
                //page++;
                //requestData = requestDataTemplate + "&start=" + (page * 48) + "&num=" + (page * 48);
                //}
            };

            // Ищем только новинки
            var reestr = LoadReestr();
            list = list.Distinct(new DataEqualityComparer()).Where(v => !reestr.Contains(v.Link)).ToList();

            // Второй проход - сбор детальной информации
            var i = 1;
            foreach (var item in list)
            {
                Console.WriteLine(string.Format("parsing {0} from {1}", i, list.Count));

                using (WebClient remote = new WebClient())
                {
                    remote.Encoding = Encoding.UTF8;
                    var htmlParser = new HtmlParser();
                    var document = htmlParser.Parse(remote.DownloadString("https://play.google.com" + item.Link));

                    var nameElement = document.QuerySelector(".id-app-title");
                    if (nameElement != null)
                        item.Name = nameElement.TextContent;

                    foreach (var metaItem in document.QuerySelectorAll(".details-section-contents .meta-info"))
                    {
                        var title = metaItem.QuerySelector(@"[class=""title""]");
                        var content = metaItem.QuerySelector(@"[class=""content""]");
                        var contentLinks = metaItem.QuerySelector(@"[class=""content contains-text-link""]");

                        if (title != null && title.TextContent == "Количество установок" && content != null)
                        {
                            item.DownloadCount = content.TextContent;
                        }

                        if (title != null && title.TextContent == "Разработчик" && contentLinks != null)
                        {
                            // Почта
                            var emailElement = contentLinks.QuerySelector(@"[href^=""mailto:""]");
                            if (emailElement != null)
                            {
                                item.DeveloperEmail = (emailElement.GetAttribute("href") + "").Replace("mailto:", "");
                            }

                            // Сайт разработчика
                            var websiteElement = contentLinks.QuerySelector(@"[href^=""https://www.google.com/url""]");
                            if (websiteElement != null)
                            {
                                var websiteLink = websiteElement.GetAttribute("href");
                                if (!string.IsNullOrWhiteSpace(websiteLink))
                                {
                                    // Ex: https://www.google.com/url?q=http://supercell.helpshift.com/a/clash-royale/&sa=D&usg=AFQjCNEcjDY02MgHR333A8cRg06Wix3jsA
                                    var websiteUri = new Uri(websiteLink);
                                    item.DeveloperWebsite = HttpUtility.ParseQueryString(websiteUri.Query).Get("q");
                                }
                            }

                            // Адрес
                            var addressElement = contentLinks.QuerySelector(@"[class=""content physical-address""]");
                            if (addressElement != null)
                            {
                                item.DeveloperAddress = (addressElement.TextContent + "").Replace(char.ConvertFromUtf32(10), " ");
                            }
                        }
                    }

                    var descriptionElement = document.QuerySelector(".show-more-content.text-body");
                    if (descriptionElement != null)
                        item.Description = descriptionElement.TextContent;
                };

                i++;

                // todo: remove
                //break;
            }

            return list;
        }

        public static List<Data> ParseCategory(string url)
        {
            var list = new List<Data>();

            var isLoadNextPage = true;
            var page = 1;

            while (isLoadNextPage)
            {
                using (WebClient remote = new WebClient())
                {
                    #region
                    remote.Headers["accept"] = "*/*";
                    remote.Headers["accept-encoding"] = "utf-8";
                    remote.Headers["accept-language"] = "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7";
                    remote.Headers["cache-control"] = "no-cache";
                    remote.Headers["content-type"] = "application/x-www-form-urlencoded;charset=UTF-8";
                    remote.Headers["cookie"] = "CONSENT=YES+RU.ru+20170625-09-0; SID=_gR24lqY-_JnAiwlocdmpKNnRwlkJqybv1yU_804v29H6xdnBQShoSIiYQ5wINzfsz3_KA.; HSID=AbHvAtZO0PNF6sT0r; SSID=AaxsU5KojHUuTXjJs; APISID=R_R4TCkVa8lxzp4-/AjouDZrOqr_DfASer; SAPISID=IewzeoMkXYQ1T2Vl/Aj2D0SrW1GLScvFht; NID=118=Tyd-co8HUvTR_22edkj-NsMqXThNR9Em10rQ4kEe1AZ-zPqHaNDX1V4XGbyiyWTBGyYwKaBTeB3aBG9e_IE4-knpNnpTtNNeCLemiln8LdClsJBb_2sa__sIvW9gCzedKSi8I5jy0Q9kP2G5IyMwlEmZwh1amFusdKa4y67Ij9do2NKlHSO5nOwD3vUd15WtccqYFlZmSxXARnPGhjs1GMVImKY5M6ivX8GSzKEbp1R4nuPEEKPv2uuE_FN-hK2dJsnzG5ZhOsM3KxDM2EToz-tL; S=billing-ui-v3=A3-dw0i6wBLh_7nKdTBpCse9w3kD4tWa:billing-ui-v3-efe=A3-dw0i6wBLh_7nKdTBpCse9w3kD4tWa; PLAY_PREFS=CtgKCPbEi6fTCRLOCgoCUlUQwomCloAsGo8KERITFBUWGNQB1QGrAuEDwgTjBeUF6AXXBtgG3gbfBpCVgQaRlYEGkpWBBpWVgQaXlYEGpJWBBriVgQbAlYEGwZWBBsSVgQbFlYEGyJWBBs6VgQbPlYEG0JWBBtSVgQbYlYEG8ZWBBviVgQb5lYEGhpaBBoeWgQaMloEGjpaBBpGWgQaeloEGn5aBBqGWgQakloEGppaBBqeWgQbul4EG75eBBoGYgQaFmIEGiZiBBouYgQa-mIEGq5uBBq2bgQbJm4EGypuBBsubgQbVm4EGvJ2BBt2dgQbnnYEGkJ6BBuKigQbzooEG_KKBBoujgQaapIEGsqSBBuqlgQbGpoEG1KaBBtamgQbPp4EGvKyBBtavgQaHsoEGibKBBtaygQaxtIEG1rmBBo7AgQaiwIEGwMCBBsHAgQbywIEG1sKBBozFgQaPxYEG-MeBBqrKgQbYzIEG3MyBBt3NgQaGzoEGoc-BBsTSgQaV1YEG2tiBBuLYgQaT2YEGy9mBBvLbgQbY5IEG3OSBBpflgQa46IEGz-uBBrDsgQbX9YEGuvuBBr__gQbE_4EGyP-BBtWDggbIhIIGuYaCBqaHgganh4IG7IeCBu2Hggb7jYIGiY6CBsyRggaVmIIGj5qCBpmaggbBmoIG95qCBp2eggbVnoIGuqCCBrugggb2ooIG4qSCBpKlggarpYIGzaWCBvKnggaeqIIGtKiCBoG0ggaDtIIGhrSCBq22ggbCu4IGj7-CBurAgga8wYIGkMuCBpHLggbNy4IG0cuCBtzMggbY0IIG89GCBtvTggaB2IIGhNqCBo7aggab2oIGo9qCBq3bggau24IGxduCBpfcggax3IIG6t2CBvjdggbv34IGp-GCBtDhggbk4YIG5eGCBoXoggaW6YIGhuuCBqPtggaF7oIGmu6CBrPuggaM8IIGsfCCBpbxgga-8YIG6_aCBq34ggaz-IIG9vqCBt77ggbf-4IG4_uCBoX8ggbC_IIG2_yCBtz8ggb__IIGgP2CBoH9ggaC_YIGgv-CBoCAgwbcgYMG8oGDBuOEgwbqhIMGkIWDBrmFgwabiIMGnYiDBtCIgwbwiIMG842DBoWPgwaQj4MGz5CDBt-QgwbZkYMG25GDBv6Rgwbbk4MG4pODBquVgwaslYMGuJWDBp2WgwaeloMGpJaDBsCWgwbWloMG45aDBtyXgwbkmYMG55mDBtuagwaZm4MG7ZuDBu6bgwbjnIMG9J6DBpWfgwbGn4MG05-DBtmfgwaYoIMGm6CDBrOggwb9oIMGwqGDBsyhgwbNoYMG1qKDBrmjgwbMp4MG7aiDBvOogwb_q4MG7K-DBvuvgwb9r4MGlrCDBrixgwa9sYMGvLSDBtC1gwa4toMGo7eDBqW4gwauuIMGubiDBsO4gwbgvIMG9LyDBvW8gwb2vIMGr76DBs2-gwbnv4MGw8GDBvnBgwb6wYMGycKDBo7DgwauxIMG5MaDBq3IgwbNyIMGnsmDBovKgwabyoMGnMqDBo_Lgwbzy4MGkMyDBsTMgwaHzYMG-M2DBofOgwaUzoMGl86DBunOgwaAz4MG6NCDBrrRgwb504MGgdSDBtzUgwbr1IMGttaDBsDWgwbX1oMG_daDBorXgwaa2IMGk9qDBqTagwau2oMGj9uDBondgwaK3YMGjt-DBpzhgwaK4oMGm-KDBqHigwby4oMGouODBrjjgwbo5IMGh-aDBuXogwYojJDR9f8rOiRjNTAxZWM3ZS02NDhjLTQ0YmEtOGU3ZS02M2FmYTNlNjcyMDlAAUgA:S:ANO1ljL4ErkTQ4_DaA; _ga=GA1.3.734552973.1493814740; _gid=GA1.3.731475328.1511806781; 1P_JAR=2017-11-28-13; SIDCC=AE4kn7829Lu-kJeYSKDaYQgZxMm1iI4ru46wsi8BF--YxK-xZAzc--FyWubM4Qp60w9jaQpH8FKWJMSuDIy74Q; _gat=1";
                    remote.Headers["origin"] = "https://play.google.com";
                    remote.Headers["pragma"] = "no-cache";
                    remote.Headers["referer"] = "https://play.google.com/store/apps/collection/promotion_30017ea_starterkit_games?clp=Sp4BCigKInByb21vdGlvbl8zMDAxN2VhX3N0YXJ0ZXJraXRfZ2FtZXMQBxgDEgRHQU1FGmwKZm5ld19ob21lX2RldmljZV9mZWF0dXJlZF9yZWNzMl90b3BpY192MV9sYXVuY2hfR0FNRV9hcHBzX19fR0FNRV8xNDk5MzY3NjAwMDAwXzFfcHJvbW9fMTUxMDg2MTM1NzMyNjAwMBAMGAM%3D:S:ANO1ljKq_P8";
                    remote.Headers["user-agent"] = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36";
                    remote.Headers["x-chrome-uma-enabled"] = "1";
                    remote.Headers["x-client-data"] = "CIi2yQEIpbbJAQjEtskBCPqcygEIqZ3KAQioo8oB";

                    #endregion

                    string requestData = string.Empty;

                    Uri uriOrigin = new Uri(url);

                    // В зависимости от страницы параметры запроса
                    if(page == 1)
                    {
                        requestData = "ipf=1&xhr=1&token=uMHUPFwvNv1DNf0CdJ3MtRYJZ-s%3A1511939780883";
                    }
                    else
                    {
                        // Все страницы по 48 элементов
                        requestData = string.Format("start={0}&num=48&numChildren=0&pagTok=gtP_uANLCkL6noGdAzwIMRDi6-aJCxDn2bZ0EIyGrfQIEMqDne0GEIeV-G8QptTc1AEQvK3VzgwQ3ceDwwIQwPO53wsQi5_aogQQwu-utIAs%3AS%3AANO1ljJXM3E&clp={1}&pagtt=3&cctcss=square-cover&cllayout=NORMAL&ipf=1&xhr=1&token=Yzd4XJbGApH6HfZ7jgy3VwSpNKQ%3A1511938297992",
                            (page - 1) * 48 - 1, HttpUtility.ParseQueryString(uriOrigin.Query).Get("clp"));
                    }

                    // В зависимости от страницы загружаем разные адреса
                    var urlToLoad = "";
                    if(page == 1)
                    {
                        urlToLoad = url;
                    }
                    else
                    {
                        urlToLoad = "https://play.google.com" + uriOrigin.AbsolutePath + "?authuser=0";
                    }

                    var htmlParser = new HtmlParser();
                    byte[] byteArray = Encoding.UTF8.GetBytes(requestData);
                    byte[] responseArray = remote.UploadData(urlToLoad, "POST", byteArray);
                    var html = Encoding.UTF8.GetString(responseArray);
                    var document = htmlParser.Parse(html);

                    var prevPageGames = list.Select(v => v.Link).ToList();

                    var anchors = document.QuerySelectorAll(".card-click-target");

                    if(anchors.Length == 0)
                    {
                        isLoadNextPage = false;
                        break;
                    }

                    foreach (var item in anchors)
                    {
                        var gameLink = item.GetAttribute("href");

                        // Если такая игра уже была, значит повторная загрузка страницы - больше нет
                        if(prevPageGames.Contains(gameLink))
                        {
                            isLoadNextPage = false;
                            break;
                        }

                        list.Add(new Data()
                        {
                            Link = gameLink
                        });

                        // todo: remove
                        //break;
                    }
                };

                // Идем на следующую страницу
                page++;
            }

            // Ищем только новинки
            var reestr = LoadReestr();
            list = list.Distinct(new DataEqualityComparer()).Where(v => !reestr.Contains(v.Link)).ToList();

            // Второй проход - сбор детальной информации
            var i = 1;
            foreach (var item in list)
            {
                Console.WriteLine(string.Format("parsing {0} from {1}", i, list.Count));

                using (WebClient remote = new WebClient())
                {
                    remote.Encoding = Encoding.UTF8;
                    var htmlParser = new HtmlParser();
                    var document = htmlParser.Parse(remote.DownloadString("https://play.google.com" + item.Link));

                    var nameElement = document.QuerySelector(".id-app-title");
                    if (nameElement != null)
                        item.Name = nameElement.TextContent;

                    foreach (var metaItem in document.QuerySelectorAll(".details-section-contents .meta-info"))
                    {
                        var title = metaItem.QuerySelector(@"[class=""title""]");
                        var content = metaItem.QuerySelector(@"[class=""content""]");
                        var contentLinks = metaItem.QuerySelector(@"[class=""content contains-text-link""]");

                        if (title != null && title.TextContent == "Количество установок" && content != null)
                        {
                            item.DownloadCount = content.TextContent;
                        }

                        if (title != null && title.TextContent == "Разработчик" && contentLinks != null)
                        {
                            // Почта
                            var emailElement = contentLinks.QuerySelector(@"[href^=""mailto:""]");
                            if (emailElement != null)
                            {
                                item.DeveloperEmail = (emailElement.GetAttribute("href") + "").Replace("mailto:", "");
                            }

                            // Сайт разработчика
                            var websiteElement = contentLinks.QuerySelector(@"[href^=""https://www.google.com/url""]");
                            if (websiteElement != null)
                            {
                                var websiteLink = websiteElement.GetAttribute("href");
                                if (!string.IsNullOrWhiteSpace(websiteLink))
                                {
                                    // Ex: https://www.google.com/url?q=http://supercell.helpshift.com/a/clash-royale/&sa=D&usg=AFQjCNEcjDY02MgHR333A8cRg06Wix3jsA
                                    var websiteUri = new Uri(websiteLink);
                                    item.DeveloperWebsite = HttpUtility.ParseQueryString(websiteUri.Query).Get("q");
                                }
                            }

                            // Адрес
                            var addressElement = contentLinks.QuerySelector(@"[class=""content physical-address""]");
                            if (addressElement != null)
                            {
                                item.DeveloperAddress = (addressElement.TextContent + "").Replace(char.ConvertFromUtf32(10), " ");
                            }
                        }
                    }

                    var descriptionElement = document.QuerySelector(".show-more-content.text-body");
                    if (descriptionElement != null)
                        item.Description = descriptionElement.TextContent;
                };

                i++;

                // todo: remove
                //break;
            }

            return list;
        }

        public static void SaveReport(List<Data> list, string category)
        {
            //using (StreamWriter sw = new StreamWriter("report." + category + ".csv", true, Encoding.UTF8))
            using (StreamWriter sw = new StreamWriter("report.csv", true, Encoding.UTF8))
            {
                var i = 0;
                foreach (var item in list)
                {
                    if (i % 30 == 0)
                    {
                        sw.WriteLine(); sw.WriteLine();
                        sw.WriteLine(@"Название;Ссылка;Кол-во скачиваний;Почта разраб.;Адрес разраб.;Сайт разраб.;Описание");
                    }

                    var fbLink = "";
                    var vkLink = "";

                    if (!string.IsNullOrWhiteSpace(item.Description) && item.Description.Contains("facebook.com"))
                    {
                        var fbLinkMatch = Regex.Match(item.Description, @"[^\s]+facebook\.com/[^\s]+");
                        if (fbLinkMatch.Success)
                            fbLink = fbLinkMatch.Value;
                    }

                    if (!string.IsNullOrWhiteSpace(item.Description) && item.Description.Contains("vk.com"))
                    {
                        var vkLinkMatch = Regex.Match(item.Description, @"[^\s]+vk\.com/[^\s]+");
                        if (vkLinkMatch.Success)
                            vkLink = vkLinkMatch.Value;
                    }

                    sw.WriteLine(string.Format(@"{0};https://play.google.com{1};{2};{3};{4};{5};{6}",
                        item.Name, item.Link, (item.DownloadCount + "").Trim(), item.DeveloperEmail, item.DeveloperAddress, item.DeveloperWebsite,

                        // Описание, только если есть ссылка на соцсети (фб, вк)
                        /*!string.IsNullOrWhiteSpace(item.Description) && (item.Description.Contains("facebook.com") || item.Description.Contains("vk.com")) ? item.Description : "")*/
                        string.Join(", ", (new string[] { fbLink, vkLink }).Where(s => !string.IsNullOrWhiteSpace(s)))

                        ));

                    i++;
                }
            }
        }

        public static void SaveReestr(List<Data> list)
        {
            var reestr = LoadReestr();
            var resultReestr = new List<string>();
            resultReestr.AddRange(reestr);
            resultReestr.AddRange(list.Select(v => v.Link));

            using (var sw = File.CreateText("reestr.txt"))
            {
                var i = 0;
                foreach (var item in resultReestr)
                {
                    sw.WriteLine(item);

                    i++;
                }
            }
        }

        public static string[] LoadReestr()
        {
            if (!File.Exists("reestr.txt"))
                return new string[] { };

            List<string> result = new List<string>();

            using (var reestr = File.OpenText("reestr.txt"))
            {
                string line = null;
                while ((line = reestr.ReadLine()) != null)
                {
                    if (string.IsNullOrWhiteSpace(line))
                        continue;

                    result.Add(line);
                }
                
            }

            return result.ToArray();
        }
    }
}
